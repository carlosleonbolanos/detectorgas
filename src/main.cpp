/* DETECTOR DE GAS CON AVISO Y TELECONTROL POR GSM
   ===============================================

 - Autor:
	Carlos León Bolaños (CIAL: A84A07009B)

 - Materia:
	Proyecto de sistemas de telecomunicaciones e informáticos (PMU)

 - Curso:
	2º CFGS Sistemas de Telecomunicaciones e Informática

 - Centro:
	C.I.F.P. César Manrique

 - Descripción:
	Sistema de detección y aviso por escape de gas y control a través de sistema SMS. Este código fuente está preparado para un sistema Arduino con los siguientes elementos:
	* Tinysine GSM/GRPS Shield con controlador SimCom SIM900.
	* Sensor de calidad de Aire FC-22 con sensor MQ-135.
	* Display 2004A.
	* Interfaz I2C para Display.
	* Zumbador piezoeléctrico Velleman VMA319.
	* Pulsador normalmente abierto (NO - Normaly Open).
	* Led 5mm rojo difuso.
	* Led 10mm RGB.
	* 2 x Resistencias 27ohms 0.25W
	Este código fue desarrollado en Microsoft(tm) Visual Studio Code (https://code.visualstudio.com/) con la extensión PlatformIO(tm) (https://platformio.org/) y alojado en GITLAB (https://gitlab.com/carlosleonbolanos/detectorgas/tree/master). Para más detalles, consultar el archivo README.md o README.pdf para más detalles.

 - Licencia:
	Licencia GNU AGPLv3 - Copyright (c) 2019 Carlos León Bolaños (más detalles contenidos en el archivo LICENSE o en https://www.gnu.org/licenses/agpl-3.0.en.html)

 - Licencias de Librerías externas utilizadas:
	* GSM Library by Farid Inawan <frdteknikelektro@gmail.com> bajo Licencia The MIT License (MIT) - Copyright (c) 2016 Atnic (más detalles en https://github.com/Atnic/GSM-library)
	* LiquidCrystal_I2C by Frank de Brabander - Más detalles en https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
	* Thread by Ivan Seidel <ivanseidel@gmail.com> bajo Licencia The MIT License (MIT) - Copyright (c) 2015 Ivan Seidel (más detalles en https://github.com/ivanseidel/ArduinoThread)
*/

//-= DECLARACIONES DE REFERENCIAS A LIBRERÍAS=-
#include <Arduino.h>						// Microprocesadores ATMEL con bootloader Arduino
#include <Thread.h>							// Creación de hilos de procesamiento paralelos
#include <ThreadController.h>		// Control de hilos de procesamiento paralelos
#include <LiquidCrystal_I2C.h>	// Uso de displays de 16x2, 16x4 o 20x4 por medio del protocolo de comunicación I2C
#include <Wire.h>								// Protocolo de comunicación I2C
#include <SoftwareSerial.h>			// Comunicación por protocolo Serial por software.
#include <DTE.h>								// Comandos AT para el SIM900
#include <GSM.h>								// Control y estado de la red GSM
#include <SMS.h>								// Control de envío y recepción de SMS
#include <URC.h>								// Análisis datos SMS
#include <GPRS.h>								// Gestión de conexión GPRS
#include <IP.h>									// Interfaz IP
#include <HTTP.h>								// Protocolo comunicación HTTP
#include <BlynkSimpleTinyGSM.h> // interfaz de control mediante aplicación Android e IOS Blynk

//-= DECLARACIONES DE DEFINICIONES =-

// - Definiciones LED
#define LED_RED 10	 // Led indicativo para aviso de detección de gas
#define LED_GREEN 11 // Led indicativo de programa en funcionamiento
#define LED_BLUE 12	// Led indicativo de lectura realizada
#define LED_PWR 4		 // Led indicativo de alimentación del Shield de GSM

// - Definiciones GAS
#define GAS_SENSOR 8 //Pin para la lectura de datos digitales del sensor
#define GAS_ALARM 9	//Pin para activación del zumbador piezoeléctrico

// - Definiciones GSM
#define GSM_DEBUG true //Definición para (des)activar la depuración
#define GSM_SSTX 3		 //Pin para canal de transmisión en Protocolo de comunicación SERIAL
#define GSM_SSRX 2		 //Pin para canal de recepción en Protocolo de comunicación SERIAL

// - Definiciones LCD
#define LCD_ADDRESS 0x3f //Dirección de la interfaz del display
#define LCD_COLS 20			 //Número de Columnas o Caractéres por línea
#define LCD_ROWS 4			 //Número de Líneas

// - Definición RESET
#define RESET 0 //Pin para el reseteo del Arduino a través de SMS

//-= DECLARACIONES DE VARIABLES =-

// - Variables LCD
LiquidCrystal_I2C LCD(LCD_ADDRESS, LCD_COLS, LCD_ROWS); // Inicializando variable para el control del display
const String LCD_R1 = "= DETECTOR GAS CLB =";
const String LCD_R2 = "Carlos León Bolaños.";

// - Variables GSM
const String GSM_SECURE = "+34638193244";				//Definiendo número de teléfono autorizado
const String KEY_HELP = "0";										//Código para mostrar la lista de códigos para las operaciones
const String KEY_READ = "1";										//Código para realizar una lectura del sensor
																								//En caso de ir acompaña por un número, este define el tiempo en milisegundos entre el envío de una lectura y otra
																								//En caso de estar acompañado de un tercer número, este indica el número de veces que realiza la acción
const String KEY_ACK = "2";											//Código para confirmar la lectura del aviso
																								//Si no se realiza esta confirmación, el programa no dejará de enviar avisos SMS y no desactivara la alarma sonora
const String KEY_ALARMTEST = "3";								//Realiza la prueba de la alarma sonora
const String KEY_SNDSMSTEST = "4";							//Envía un SMS de prueba al número autorizado
const String KEY_RCVSMSTEST = "5";							//Envía un SMS indicando si el número está autorizado o no
const String KEY_GSMCOVER = "6";								//Transmite el nivel de cobertura
const String KEY_SMTPTEST = "7";								//Comprueba el envío de un correo electrónico a una dirección predefinida
const String KEY_HTTPTEST = "8";								//Comprueba la conexión HTTP a través de GPRS
const String KEY_PGUP = "-";										//Envía las opciones anteriormente mostradas
const String KEY_PGDOWN = "+";									//Envía el siguiente grupo de opciones
const String KEY_RESET = "999";									//Rearme de la alarma
SoftwareSerial GSM_SSERIAL(GSM_SSRX, GSM_SSTX); //Variable de control comunicación serial
DTE GSM_DTE(GSM_SSERIAL, LED_PWR, GSM_DEBUG);		//Variable para la comunicación con el SIM900
SMS GSM_SMS(GSM_DTE);														//Variable para el flujo de datos SMS
GSM GSM_GSM(GSM_DTE);														//Variable para control variables GSM

//TODO: Hacer función para hacer hilos de lecturas y recordatorios periódicas/os
//TODO: Añadir símbolos para cobertura

//-= CONFIGURACIÓN INICIAL =-
void setup()
{
	Serial.begin(115200);
	// Establecer
	LCD.setCursor(0, 1);
	LCD.println(LCD_R1);
	LCD.println(LCD_R2);
	GSM_SSERIAL.begin(19200);
}

//-=
void loop()
{
	// put your main code here, to run repeatedly:
}
/* DETECTOR DE GAS CON AVISO Y TELECONTROL POR GSM
   ===============================================

	- Autor:
		Carlos León Bolaños (CIAL: A84A07009B)

 	- Materia:
		Proyecto de sistemas de telecomunicaciones e informáticos (PMU)

 	- Curso:
		2º CFGS Sistemas de Telecomunicaciones e Informática

 	- Centro:
		C.I.F.P. César Manrique

	- Descripción:
    Cabecera de simbología para personalización de la pantalla. Alojado en GITLAB (https://gitlab.com/carlosleonbolanos/detectorgas/tree/Simple)

 	- Licencia:
		Licencia GNU AGPLv3 - Copyright (c) 2019 Carlos León Bolaños (más detalles contenidos en el archivo LICENSE o en https://gitlab.com/carlosleonbolanos/detectorgas/blob/Simple/LICENSE)
*/

#ifndef Symbols_h
#define Symbols_h

//-= DECLARACIONES DE REFERENCIAS A LIBRERÍAS=-
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

// - Definiciones LCD
#define LCD_ADDRESS 0x3f // Dirección I2C de la interfaz del display
#define LCD_COLS 20			 // Número de Columnas o Caractéres por línea
#define LCD_ROWS 4			 // Número de Líneas

// -= CARACTERES PERSONALIZADOS =-

// Título del proyecto
byte TIT_000[] = {0x1E, 0x1E, 0x1B, 0x1B, 0x1B, 0x1E, 0x1E, 0x00}; // 0
byte TIT_010[] = {0x1F, 0x1F, 0x18, 0x1E, 0x18, 0x1F, 0x1F, 0x00}; // 1
byte TIT_020[] = {0x1F, 0x1F, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x00}; // 2
byte TIT_030[] = {0x1F, 0x1F, 0x18, 0x1E, 0x18, 0x1F, 0x1F, 0x00}; // 3
byte TIT_040[] = {0x0F, 0x1F, 0x18, 0x18, 0x18, 0x1F, 0x0F, 0x00}; // 4
byte TIT_050[] = {0x1F, 0x1F, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x00}; // 5
byte TIT_060[] = {0x0E, 0x1F, 0x1B, 0x1B, 0x1B, 0x1F, 0x0E, 0x00}; // 6
byte TIT_070[] = {0x1E, 0x1F, 0x1B, 0x1E, 0x1F, 0x1F, 0x1B, 0x00}; // 7
byte TIT_090[] = {0x0E, 0x1F, 0x18, 0x1B, 0x1B, 0x1F, 0x0E, 0x00}; // 8
byte TIT_100[] = {0x0E, 0x1F, 0x1B, 0x1F, 0x1F, 0x1B, 0x1B, 0x00}; // 9
byte TIT_110[] = {0x0E, 0x1F, 0x18, 0x1F, 0x03, 0x1F, 0x0E, 0x00}; // 10
byte TIT_130[] = {0x0F, 0x1F, 0x18, 0x18, 0x18, 0x1F, 0x0F, 0x00}; // 11
byte TIT_140[] = {0x18, 0x18, 0x18, 0x18, 0x18, 0x1F, 0x1F, 0x00}; // 12
byte TIT_150[] = {0x1E, 0x1F, 0x1B, 0x1E, 0x1B, 0x1F, 0x1E, 0x00}; // 13
byte *TIT[14][8] = {TIT_000, TIT_010, TIT_020, TIT_030, TIT_040, TIT_050, TIT_060, TIT_070, TIT_090, TIT_100, TIT_110, TIT_130, TIT_140, TIT_150};

// Cobertura
byte SIGNALSYM_180[] = {0x00, 0x0E, 0x11, 0x04, 0x0A, 0x00, 0x04, 0x00}; // 14
byte COV45_190[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05};		 // 15
byte COV4_190[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04};			 // 16
byte COV12_181[] = {0x00, 0x00, 0x00, 0x02, 0x02, 0x0A, 0x0A, 0x00};		 // 17
byte COV1_181[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x08, 0x00};			 // 18
byte COV345_191[] = {0x05, 0x15, 0x15, 0x15, 0x15, 0x15, 0x15, 0x00};		 // 19
byte COV34_191[] = {0x04, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x00};		 // 20
byte COV3_191[] = {0x00, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x00};			 // 21
byte NOCOV_180[] = {0x00, 0x01, 0x06, 0x08, 0x08, 0x10, 0x11, 0x0A};		 // 22
byte NOCOV_190[] = {0x00, 0x10, 0x0C, 0x06, 0x0A, 0x11, 0x01, 0x02};		 // 23
byte NOCOV_181[] = {0x0C, 0x06, 0x01, 0x00, 0x1D, 0x11, 0x14, 0x1D};		 // 24
byte NOCOV_191[] = {0x02, 0x0C, 0x10, 0x00, 0x15, 0x07, 0x15, 0x15};		 // 25
byte *COV[12][8] = {SIGNALSYM_180, COV45_190, COV4_190, COV12_181, COV1_181, COV345_191, COV34_191, COV3_191, NOCOV_180, NOCOV_190, NOCOV_181, NOCOV_191};

//Conexión
byte GPRS_162[] = {0x00, 0x00, 0x03, 0x04, 0x04, 0x05, 0x05, 0x03};		// 26
byte GPRS_172[] = {0x00, 0x00, 0x18, 0x14, 0x14, 0x18, 0x10, 0x10};		// 27
byte GPRS_163[] = {0x06, 0x05, 0x05, 0x06, 0x05, 0x05, 0x00, 0x00};		// 28
byte GPRS_173[] = {0x0C, 0x10, 0x08, 0x04, 0x04, 0x18, 0x00, 0x00};		// 29
byte DOWN_182[] = {0x00, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04};		// 30
byte UP_192[] = {0x00, 0x04, 0x0E, 0x1F, 0x04, 0x04, 0x04, 0x04};			// 31
byte DOWNOK_183[] = {0x1F, 0x0E, 0x04, 0x00, 0x0E, 0x0A, 0x0A, 0x0E}; // 32
byte UPOK_193[] = {0x04, 0x04, 0x04, 0x00, 0x0A, 0x0C, 0x0A, 0x0A};		// 33
byte NOGPRS_182[] = {0x00, 0x01, 0x06, 0x08, 0x08, 0x10, 0x11, 0x0A}; // 34
byte NOGPRS_192[] = {0x00, 0x10, 0x0C, 0x06, 0x0A, 0x11, 0x01, 0x02}; // 35
byte NOGPRS_183[] = {0x0C, 0x06, 0x01, 0x00, 0x0E, 0x0A, 0x0A, 0x0A}; // 36
byte NOGPRS_193[] = {0x02, 0x0C, 0x10, 0x00, 0x0E, 0x0A, 0x0A, 0x0E}; // 37
byte *CONN[12][8] = {GPRS_162, GPRS_172, GPRS_163, GPRS_173, DOWN_182, UP_192, DOWNOK_183, UPOK_193, NOGPRS_182, NOGPRS_192, NOGPRS_183, NOGPRS_193};

// SMS
byte SMS_003[] = {0x00, 0x0F, 0x0C, 0x0A, 0x09, 0x08, 0x0F, 0x00}; // 38
byte SMS_013[] = {0x00, 0x1E, 0x06, 0x0A, 0x12, 0x02, 0x1E, 0x00}; // 39
byte LARROW[] = {0x00, 0x04, 0x0C, 0x1F, 0x0C, 0x04, 0x00, 0x00};	// 40
byte RARROW[] = {0x00, 0x04, 0x06, 0x1F, 0x06, 0x04, 0x00, 0x00};	// 41
byte LINE[] = {0x00, 0x00, 0x00, 0x1F, 0x00, 0x00, 0x00, 0x00};		 // 42
byte *SMS[5][8] = {SMS_003, SMS_013, LARROW, RARROW, LINE};

//Alarma visual y acustica
byte GAS_000[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x03};	// 43
byte GAS_010[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C};	// 44
byte GAS_020[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F};	// 45
byte GAS_030[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11};	// 46
byte GAS_040[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F};	// 47
byte GAS_110[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0F};	// 48
byte GAS_120[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x11};	// 49
byte GAS_130[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1E};	// 50
byte GAS_140[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07};	// 51
byte GAS_150[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C};	// 52
byte GAS_001[] = {0x07, 0x06, 0x06, 0x06, 0x06, 0x06, 0x06, 0x07};	// 53
byte GAS_011[] = {0x1E, 0x06, 0x00, 0x00, 0x1E, 0x1E, 0x06, 0x1E};	// 54
byte GAS_021[] = {0x1F, 0x18, 0x18, 0x18, 0x1F, 0x1F, 0x18, 0x18};	// 55
byte GAS_031[] = {0x1B, 0x1B, 0x1B, 0x19, 0x19, 0x18, 0x18, 0x1B};	// 56
byte GAS_041[] = {0x1F, 0x00, 0x00, 0x1E, 0x1E, 0x03, 0x03, 0x1F};	// 57
byte GAS_111[] = {0x1F, 0x18, 0x18, 0x18, 0x1B, 0x1B, 0x18, 0x1F};	// 58
byte GAS_121[] = {0x1B, 0x1B, 0x03, 0x03, 0x1B, 0x1B, 0x1B, 0x1B};	// 59
byte GAS_131[] = {0x1F, 0x03, 0x03, 0x03, 0x1F, 0x1F, 0x03, 0x03};	// 60
byte GAS_141[] = {0x0F, 0x0C, 0x0C, 0x07, 0x07, 0x00, 0x00, 0x0F};	// 61
byte GAS_151[] = {0x1C, 0x00, 0x00, 0x18, 0x18, 0x0C, 0x0C, 0x1C};	// 62
byte GAS_002[] = {0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 63
byte GAS_012[] = {0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 64
byte GAS_022[] = {0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 65
byte GAS_032[] = {0x1B, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 66
byte GAS_042[] = {0x1E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 67
byte GAS_112[] = {0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 68
byte GAS_122[] = {0x13, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 69
byte GAS_132[] = {0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 70
byte GAS_142[] = {0x0F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 71
byte GAS_152[] = {0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};	// 72
byte SIGN_051[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01}; // 73
byte SIGN_052[] = {0x01, 0x03, 0x03, 0x06, 0x06, 0x0C, 0x0F, 0x07}; // 74
byte SIGN_060[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03}; // 75
byte SIGN_061[] = {0x03, 0x06, 0x06, 0x0C, 0x0C, 0x18, 0x18, 0x10}; // 76
byte SIGN_062[] = {0x10, 0x01, 0x01, 0x00, 0x00, 0x00, 0x1F, 0x1F}; // 77
byte SIGN_070[] = {0x03, 0x0F, 0x0C, 0x18, 0x18, 0x10, 0x10, 0x01}; // 78
byte SIGN_071[] = {0x01, 0x03, 0x03, 0x01, 0x05, 0x0E, 0x0E, 0x1E}; // 79
byte SIGN_072[] = {0x1C, 0x1C, 0x1E, 0x1E, 0x1E, 0x0D, 0x1F, 0x1F}; // 80
byte SIGN_080[] = {0x18, 0x1E, 0x06, 0x03, 0x03, 0x01, 0x11, 0x10}; // 81
byte SIGN_081[] = {0x18, 0x18, 0x18, 0x1C, 0x1E, 0x1F, 0x1F, 0x0F}; // 82
byte SIGN_082[] = {0x0F, 0x0F, 0x1F, 0x1F, 0x1F, 0x1E, 0x1F, 0x1F}; // 83
byte SIGN_090[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x10, 0x18}; // 84
byte SIGN_091[] = {0x18, 0x0C, 0x0C, 0x06, 0x06, 0x03, 0x03, 0x11}; // 85
byte SIGN_092[] = {0x11, 0x18, 0x18, 0x10, 0x00, 0x00, 0x1F, 0x1F}; // 86
byte SIGN_101[] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10}; // 87
byte SIGN_102[] = {0x10, 0x18, 0x18, 0x0C, 0x0C, 0x06, 0x1E, 0x1C}; // 88
byte BELL_053[] = {0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x01, 0x00}; // 89
byte BELL_063[] = {0x00, 0x04, 0x09, 0x09, 0x09, 0x09, 0x04, 0x00}; // 90
byte BELL_073[] = {0x00, 0x11, 0x02, 0x02, 0x02, 0x04, 0x17, 0x00}; // 91
byte BELL_083[] = {0x00, 0x11, 0x08, 0x08, 0x08, 0x04, 0x1D, 0x00}; // 92
byte BELL_093[] = {0x00, 0x04, 0x12, 0x12, 0x12, 0x12, 0x04, 0x00}; // 93
byte BELL_103[] = {0x00, 0x10, 0x08, 0x08, 0x08, 0x08, 0x10, 0x00}; // 94
byte *ALARM[53][8] = {GAS_000, GAS_010, GAS_020, GAS_030, GAS_040, GAS_110, GAS_120, GAS_130, GAS_140, GAS_150, GAS_001, GAS_011, GAS_021, GAS_031, GAS_041, GAS_111, GAS_121, GAS_131, GAS_141, GAS_151, GAS_002, GAS_012, GAS_022, GAS_032, GAS_042, GAS_112, GAS_122, GAS_132, GAS_142, GAS_152, SIGN_060, SIGN_070, SIGN_080, SIGN_090, SIGN_051, SIGN_061, SIGN_071, SIGN_081, SIGN_091, SIGN_052, SIGN_062, SIGN_072, SIGN_082, SIGN_092, BELL_053, BELL_063, BELL_073, BELL_083, BELL_093, BELL_103};

int CHARS_COUNT = 95;
int CHARS_COUNTER = 0;

bool CreateGasChars();
void PrintOperator(String OP);
#endif
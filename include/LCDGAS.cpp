/* DETECTOR DE GAS CON AVISO Y TELECONTROL POR GSM
   ===============================================

	- Autor:
		Carlos León Bolaños (CIAL: A84A07009B)

 	- Materia:
		Proyecto de sistemas de telecomunicaciones e informáticos (PMU)

 	- Curso:
		2º CFGS Sistemas de Telecomunicaciones e Informática

 	- Centro:
		C.I.F.P. César Manrique

	- Descripción:
    Código fuente de simbología para personalización de la pantalla. Alojado en GITLAB (https://gitlab.com/carlosleonbolanos/detectorgas/tree/Simple)

 	- Licencia:
		Licencia GNU AGPLv3 - Copyright (c) 2019 Carlos León Bolaños (más detalles contenidos en el archivo LICENSE o en https://gitlab.com/carlosleonbolanos/detectorgas/blob/Simple/LICENSE)
*/

#include <Arduino.h>
#include <LCDGAS.h>

LiquidCrystal_I2C LCD(LCD_ADDRESS, LCD_COLS, LCD_ROWS); // Inicializando variable para el control del display

bool CreateGasChars()
{
	LCD.createChar(0, TIT_000);
}

void PrintOperator(String OP)
{
	int X = 5 + (5 - (floor(OP.length / 2)));
	LCD.setCursor(X, 3);
	LCD.print(OP);
}